# W21 FS1030 – Jonathan Francis -- PERSONAL PORTFOLIO PROJECT FRONTEND

# Links below - for the W21 FS1030 – Jonathan Francis -- PERSONAL PORTFOLIO PROJECT 

*Active - For the Backend API - https://gitlab.com/Jonathan.Francis/francis-jonathan-backend-fs-1030

*Active - For the Frontend for this project - https://gitlab.com/jonathan-francis/francis_jonathan_frontend_fs1030_localhost

*Active - For the Database - https://gitlab.com/Jonathan.Francis/francis-jonathan-database-fs-1030

# Run this project using npm

1. To run this project you will need to clone this repo using SSH or HTTP
2. Open the folder in VS Code or applicable source-code editor with access to terminal
3. When in the terminal, use npm install to install node dependencies
4. The project uses http://localhost:3000 to run so ensure that port 3000 is not in use
5. In the terminal, type in 'npm start' and go to http://localhost:3000 in your preferred browser (it should open automatically)
6. Make sure you follow the Backend and Database instructions provided in the links above to get the site fully functional.
7. From here you can Navigate the React App

# Special Instructions on Flow of the Website.

1) As an Employer, you must leave an Entry on the Entry Page.
2) Once completed, you can use the same credientals to start/create a user account on the User Page but must have a unique 8 character mininum password.
3) After creating an user account, use the Email & Password to Login on the Login Page.
4) Once Logged in you must make sure to remember  your unique Employer Entry ID.
5) To CRUD Employer Feedbacks/Comments on the Resume/Portfolio Pages you will require your Employer Entry ID as it is a foreign key.

Thank you!


# Important
For this current repository we are using - http://localhost:3000/ to have our React file running, see full details below.


Getting Started with Create React App
This project was bootstrapped with Create React App.

Available Scripts
In the project directory, you can run:

npm start

Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.
The page will reload if you make edits.
You will also see any lint errors in the console.

npm test

Launches the test runner in the interactive watch mode.
See the section about running tests for more information.

npm run build

Builds the app for production to the build folder.
It correctly bundles React in production mode and optimizes the build for the best performance.
The build is minified and the filenames include the hashes.
Your app is ready to be deployed!
See the section about deployment for more information.

npm run eject

Note: this is a one-way operation. Once you eject, you can’t go back!
If you aren’t satisfied with the build tool and configuration choices, you can eject at any time. This command will remove the single build dependency from your project.
Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except eject will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.
You don’t have to ever use eject. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

Learn More
You can learn more in the Create React App documentation.
To learn React, check out the React documentation.

Code Splitting
This section has moved here: https://facebook.github.io/create-react-app/docs/code-splitting

Analyzing the Bundle Size
This section has moved here: https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size

Making a Progressive Web App
This section has moved here: https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app

Advanced Configuration
This section has moved here: https://facebook.github.io/create-react-app/docs/advanced-configuration

Deployment
This section has moved here: https://facebook.github.io/create-react-app/docs/deployment

npm run build fails to minify
This section has moved here: https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify


# EXTRA DETAILS 

Please note .gitlab-ci.yml was commented out as - #.gitlab-ci.yml. This was used during our York Full Stack Web Development in 2021 when we had access for a free-trial using GCP. This yml file was used to have Gitlab upload our React project onto GCP. Link for that is below:

Inactive - see link below for the Frontend used in the York Full Stack Program during the Free GCP Trial. This is no longer active and listed here for reference only.
https://gitlab.com/Jonathan.Francis/francis-jonathan-frontend-fs-1030.
This link was used to have Gitlab upload our React Project onto GCP, and is no longer active due to the Free GCP Trial expiring.