import React, {Component} from 'react'
import axios from "axios"
import { withRouter } from 'react-router-dom';


class Submissions extends Component {


 constructor(props) {
        super(props)
        this.state = {
            user: []
        }
    }
  
    redirectToProfile= (user) => {
        const { history } = this.props;
        if(history) history.push(`/Submissions/${user.entryID}`);
       }

        redirectToPorfolio= () => {
        const { history } = this.props;
        if(history) history.push(`/Portfolio/`);
       }

            redirectToResume= () => {
            const { history } = this.props;
            if(history) history.push(`/Resume/`);
       }


    componentDidMount() {
            const token = localStorage.getItem("token")
            console.log("This is the token", token)
            const config = {
                headers :{
                    Authorization :  `Bearer ${token}`
            }
        }
        
            axios.get("http://localhost:8080/contact_form/entries", config)
                .then(res => {
                console.log(res)
                this.setState({
                user: res.data
             })
            })
            .catch(err => console.log(err))
        }


    render() {

        
        return (
            <div>
              <h2>Welcome, You have Successfully Logged in!</h2>
              <br></br>
              <h3>Below you will find your Entry Submission & Unique Employer Entry ID in this Employer Submissions Master List</h3>
              <br></br>
              <h4>You will require your Unique Employer Entry ID To Leave Feedbacks/Comments, Thank you!</h4>
              <br></br>
              <br></br>
                            <button onClick={(this.redirectToResume)} >View Resume</button>
                            <button onClick={(this.redirectToPorfolio)} >View Portfolio</button>
                            <br></br>
                            <br></br>  
                            <br></br>
                            <br></br>             
            { 
            this.state.user.length > 0 && (
                        <div>  
                        {
                        this.state.user.map(user => (
                            
                        <div>
                        <ol>
                        <li> Employer Entry Id: {user.entryID}</li>
                        <li> Name: {user.entryName} </li>
                        <li> Email: {user.entryEmail} </li>
                        <li> Phone Number: {user.entryPhoneNumber} </li>
                        <li> Content: {user.entryContent} </li> 
                        <button onClick={(this.redirectToProfile.bind(this,user))}>View Profile</button>
                        </ol>
                        </div>
                        ))
                        }
                        </div>
            )          
            }
          </div>
    
        )
    }
}

export default withRouter(Submissions);