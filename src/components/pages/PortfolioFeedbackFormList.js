import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    table: {
      width: '100%'
    },
  });


const PortfolioFeedbackFormList = () => {


const [feedbacks,setFeedbacks] = useState([]);
const history = useHistory();
const classes = useStyles();


const resumeRoute = (event) =>{
  event.preventDefault();
  let path = "/Resume"
  history.push(path);

};

const portfolioRoute = (event) =>{
  event.preventDefault();
  let path = "/Portfolio"
  history.push(path);

};


const PortfolioProfileRoute = (event, feedback) => {
    event.preventDefault();
    let path = `/Portfolio/FeedbackForm/List/${feedback.portfolioEmployerID}`
    history.push(path);
  };


useEffect(() => {
    async function fetchData() {
      const res = await fetch("http://localhost:8080/portfolio");
      res
        .json()
        .then((res) => setFeedbacks(res))
        .catch((err) => console.log(err));
    }
    fetchData();
  }, []);



  return(
    <div>
        <h1>Portfolio Feedback Master List</h1>
        <button onClick={(e) => portfolioRoute(e)}>Return to Portfolio Page</button>
        <button onClick={(e) => resumeRoute(e)}>Visit Resume Page</button>
        <TableContainer className={classes.table} component={Paper}>
            <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                    <TableCell>ID:</TableCell>
                    <TableCell>Employer Contact</TableCell>
                    <TableCell>Employer Feedback</TableCell>
                    <TableCell>Employer Rating</TableCell>
                    <TableCell>Employer Entry ID </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {feedbacks.map((feedback) =>(
                        <TableRow key={feedback.portfolioEmployerID}>
                            <TableCell>{feedback.portfolioEmployerID}</TableCell>
                            <TableCell>{feedback.portfolioEmployerContact}</TableCell>
                            <TableCell>{feedback.portfolioEmployerFeedback}</TableCell>
                            <TableCell>{feedback.portfolioEmployerRating}</TableCell>
                            <TableCell>{feedback.entryID}</TableCell>
                            <TableCell><Button variant="contained" color="primary" onClick={(e) => PortfolioProfileRoute(e, feedback)} component={Paper} >VIEW PROFILE</Button></TableCell>   
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    </div>
);


}

export default PortfolioFeedbackFormList;