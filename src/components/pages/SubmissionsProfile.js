import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';

const useStyles = makeStyles({
  textfield: {
    marginBottom: 10
  },
  table: {
    width: '100%',
    marginBottom: 40,
  },
  dateHeader: {
    width: 205,
  },
  margin: {
    margin: "0 10px 20px 0",
    width: '130%'
  },
  subheading: {
    margin: "50px 5px 20px 0",
    textAlign: "left"
  },
});

const SubmissionsProfile = (props) => {
 

  let id = props.match.params.id;
  
  const [savedRes, setSavedRes] = useState("")
  const [entrys, setEntrys] = useState([]);
  const [form, setForm] = useState({ display: "none" });
  const [entry, setEntry] = useState({entryName: "", entryPhoneNumber:"", entryContent:"" });
  const history = useHistory();
  const classes = useStyles();


  const returnRoute = (event) =>{
    event.preventDefault();
    let path = `/Submissions`;
    history.push(path);
  };
  

  useEffect(() => {
        async function fetchData() {
            const res = await fetch(`http://localhost:8080/contact_form/entries/${id}`);
                res.json().then((res) => setEntrys(res));
             }
                    fetchData();
                            }, [savedRes, id]);



  const handleDelete = (event) => {
    event.preventDefault();

    fetch(`http://localhost:8080/contact_form/entries/${id}`, {
      method: "delete",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
    history.push("/Submissions");
  };
  


  const handleEdit = (event, entry) => {
        event.preventDefault();
            setForm({ display: "block" });
                setEntry(entry);
  };



  const handleChange = e => setEntry({...entry,[e.target.name]: e.target.value});




  const handleSubmit = (event) => {
        event.preventDefault();
            fetch(`http://localhost:8080/contact_form/entries/${id}`, {
            method: "put",
            headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
      },

                //make sure to serialize your JSON body
                    body: JSON.stringify(entry),
                  }).then((response) => setSavedRes(response));
  };



  return (
    <div>
      <h1>Submission Profile</h1>
<button onClick={(e) => returnRoute(e)}>Return</button>   
<TableContainer className={classes.table} component={Paper}>
    <Table aria-label="simple table">
        <TableHead>
            <TableRow>
            <TableCell>Employer Entry ID:</TableCell>
            <TableCell>Name:</TableCell>
            <TableCell>Email:</TableCell>
            <TableCell>Phone Number: </TableCell>
            <TableCell>Content: </TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
            {entrys.map((entry) =>(
                <TableRow key={entry.entryID}>
                    <TableCell>{entry.entryID}</TableCell>
                    <TableCell>{entry.entryName}</TableCell>
                    <TableCell>{entry.entryEmail}</TableCell>
                    <TableCell>{entry.entryPhoneNumber}</TableCell>
                    <TableCell>{entry.entryContent}</TableCell>
                    <TableCell><Button variant="contained" color="primary" onClick={(e) => {handleEdit(e, entry); }} component={Paper}>Edit</Button></TableCell>
                    <TableCell><Button variant="contained" color="secondary" onClick={(e) => {handleDelete(e);}} component={Paper}>Delete</Button></TableCell>
                </TableRow>
            ))}
        </TableBody>
    </Table>
</TableContainer>

      {/* This is the form that pops up when you press the Edit Me! button */}
      <form style={form}>
      
      <div>
            <TextField 
              fullWidth 
              className={classes.textfield} 
              variant="outlined"
              label="Name:"
              type="text"
              name="entryName"
              value={entry.entryName}
              onChange={handleChange}
            />
        </div>
        <div>
          <TextField 
              fullWidth 
              className={classes.textfield} 
              variant="outlined"
              label="Phone Number:"
              type="tel"
              name="entryPhoneNumber"
              value={entry.entryPhoneNumber}
              onChange={handleChange}
            />
        </div>
        <div>
          <TextField 
              fullWidth 
              className={classes.textfield} 
              variant="outlined"
              label="Content:"
              type="text"
              name="entryContent"
              value={entry.entryContent}
              onChange={handleChange}
            />
        </div>
        <Button className={classes.textfield} fullWidth variant="contained" color="primary" onClick={(e) => handleSubmit(e, entry)} component={Paper}>Submit</Button>
        <Button className={classes.textfield} fullWidth onClick={() => {setForm({ display: "none" })}}>Cancel</Button>
      </form>
    </div>
  );
};

export default SubmissionsProfile;


