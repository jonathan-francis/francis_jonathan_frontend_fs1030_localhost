import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { TextField, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  textfield: {
    marginBottom: 10
  },
  table: {
    width: '100%',
    marginBottom: 40,
  },
  dateHeader: {
    width: 205,
  },
  margin: {
    margin: "0 10px 20px 0",
    width: '130%'
  },
  subheading: {
    margin: "50px 5px 20px 0",
    textAlign: "left"
  },
});



const PortfolioFeedbackListProfile = (props) =>{

    let id=props.match.params.id;
    const [savedRes, setSavedRes] = useState("");
    const [feedbacks, setFeedbacks] = useState([]);
    const [form, setForm] = useState({display:"none"});
    const [feedback, setFeedback] = useState({portfolioEmployerContact:"", portfolioEmployerFeedback:"", portfolioEmployerRating:""})
    const history = useHistory();
    const classes = useStyles();

    const returnRoute = (event) =>{
      event.preventDefault();
      let path = `/Portfolio/FeedbackForm/List`;
      history.push(path);
    };
    


    useEffect(() => {
        async function fetchData(){
            const res = await fetch(`http://localhost:8080/portfolio/${id}`)
            res.json().then((res) => setFeedbacks (res));
        }
     fetchData();
    }, [savedRes, id]);

    const handleDelete = (event) => {
        event.preventDefault();
    
        fetch(`http://localhost:8080/portfolio/${id}`, {
          method: "delete",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
        }).then((response) => response.json());
        history.push("/Portfolio/FeedbackForm/List");
      };

      const handleEdit = (event, feedback) => {
        event.preventDefault();
        setForm({ display: "block" });
        setFeedback(feedback);
      }; 

 
      const handleChange = e => setFeedback({...feedback,[e.target.name]: e.target.value});

      const handleSubmit = (event) => {
        event.preventDefault();
       
        fetch(`http://localhost:8080/portfolio/${id}`, {
          method: "put",
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
          },
    
          //make sure to serialize your JSON body
          body: JSON.stringify(feedback),
        }).then((response) => setSavedRes(response));
      };


return(
    <div>
<h1>Portfolio Feedback Profile</h1>
<button onClick={(e) => returnRoute(e)}>Return</button>
<TableContainer className={classes.table} component={Paper}>
    <Table aria-label="simple table">
        <TableHead>
            <TableRow>
            <TableCell>ID:</TableCell>
            <TableCell>Employer Contact:</TableCell>
            <TableCell>Employer Feedback:</TableCell>
            <TableCell>Employer Rating: </TableCell>
            <TableCell>Employer Entry ID </TableCell>
            </TableRow>
        </TableHead>
        <TableBody>
            {feedbacks.map((feedback) =>(
                <TableRow key={feedback.portfolioEmployerID}>
                    <TableCell>{feedback.portfolioEmployerID}</TableCell>
                    <TableCell>{feedback.portfolioEmployerContact}</TableCell>
                    <TableCell>{feedback.portfolioEmployerFeedback}</TableCell>
                    <TableCell>{feedback.portfolioEmployerRating}</TableCell>
                    <TableCell>{feedback.entryID}</TableCell>
                    <TableCell><Button variant="contained" color="primary" onClick={(e) => {handleEdit(e, feedback);}} component={Paper}>Edit</Button></TableCell>
                    <TableCell><Button variant="contained" color="secondary"  onClick={(e) => {handleDelete(e);}} component={Paper}>Delete</Button></TableCell>
                </TableRow>
            ))}
        </TableBody>
    </Table>
</TableContainer>
 {/* This is the form that pops up when you press the Edit Me! button */}
 <form style={form}>
          <div>
            <TextField 
              fullWidth 
              className={classes.textfield} 
              variant="outlined"
              label="Employer Contact:"
              type="text"
              name="portfolioEmployerContact"
              value={feedback.portfolioEmployerContact}
              onChange={handleChange}
            />
         </div>
        <div>
        <TextField 
              fullWidth 
              className={classes.textfield} 
              variant="outlined"
              label="Employer Feedback:"
              type="text"
              name="portfolioEmployerFeedback"
              value={feedback.portfolioEmployerFeedback}
              onChange={handleChange}
            />
        </div>
        <div>
          <TextField 
              fullWidth 
              className={classes.textfield} 
              variant="outlined"
              label="Employer Rating:"
              type="text"
              name="portfolioEmployerRating"
              value={feedback.portfolioEmployerRating}
              onChange={handleChange}
            />
        </div>
        <Button className={classes.textfield} fullWidth variant="contained" color="primary" onClick={(e) => handleSubmit(e)} component={Paper}>Submit</Button>
        <Button className={classes.textfield} fullWidth onClick={() => {setForm({ display: "none" })}}>Cancel</Button>
        
      </form>

    </div>
)


}

export default PortfolioFeedbackListProfile;