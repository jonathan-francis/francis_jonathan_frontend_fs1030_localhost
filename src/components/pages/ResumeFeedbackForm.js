import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { TextField } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import SubmitButtton from "../shared/SubmitButton"

const useStyles = makeStyles({
  textfield: {
    marginBottom: 10
  },
});




const ResumeFeedbackForm = () => {

  const classes = useStyles();
  const [feedback, setFeedback] = useState({ resumeEmployerName: "", resumeEmployerContact: "",resumeEmployerOpening: "",resumeEmployerFeedback:"",resumeEmployerDate: "",entryID:""});
  const history = useHistory();
  

  const returnRoute = (event) =>{
    event.preventDefault();
    let path = `/Resume`;
    history.push(path);
  };
  


  const handleChange = e => setFeedback({...feedback,[e.target.name]: e.target.value});

  const handleSubmit = (event) => {
    fetch("http://localhost:8080/resume", {
      method: "post",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },

      //make sure to serialize your JSON body
      body: JSON.stringify(feedback),
    }).then((response) => response.json());
    history.push("/Resume/FeedbackForm/List");
  };

  return (
    <div>
      <h2>Employer Feedback on Resume</h2>
      <button onClick={(e) => returnRoute(e)}>Return</button>
      <br></br>
      <br></br>
      <form noValidate autoComplete="off" onSubmit={handleSubmit}>
        <TextField fullWidth className={classes.textfield} id="addEmployerName" label="Employer Name:" variant="outlined" type="text" name="resumeEmployerName" value={feedback.resumeEmployerName} onChange={handleChange} required/>
        <TextField fullWidth className={classes.textfield} id="addEmployerContactInformation" label="Employer Contact Information" variant="outlined" type="text" name="resumeEmployerContact" value={feedback.resumeEmployerContact}  onChange={handleChange} required/>     
        <TextField fullWidth className={classes.textfield} id="addEmployerJobOpening" label="Job Opening/Opportunity" variant="outlined" type="text" name="resumeEmployerOpening" value={feedback.resumeEmployerOpening}  onChange={handleChange} required/> 
        <TextField fullWidth className={classes.textfield} id="addEmployerFeedback" label="Employer Feedback:" variant="outlined" type="text" name="resumeEmployerFeedback" value={feedback.resumeEmployerFeedback}  onChange={handleChange} required/>     
        <TextField fullWidth className={classes.textfield} id="addEmployerDate" label="Date" variant="outlined" type="date" InputLabelProps={{shrink: true,}} name="resumeEmployerDate" value={feedback.resumeEmployerDate} onChange={handleChange} required/>
        <TextField fullWidth className={classes.textfield} id="addEmployerEntryID" label="Employer Entry ID" variant="outlined" type="text" name="entryID" value={feedback.entryID}  onChange={handleChange} required/>     
          <SubmitButtton />
      </form>
    </div>
  );
};




export default ResumeFeedbackForm;
