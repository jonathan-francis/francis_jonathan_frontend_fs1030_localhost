import React, { useState } from 'react';
import { Document, Page, pdfjs } from 'react-pdf';
import pdfFile from "../pdf/transcript.pdf"
pdfjs.GlobalWorkerOptions.workerSrc = `//unpkg.com/pdfjs-dist@${pdfjs.version}/legacy/build/pdf.worker.min.js`;

function Transcript() {
  const [numPages, setNumPages] = useState(null);
  const [pageNumber, setPageNumber] = useState(1);

  function onDocumentLoadSuccess({ numPages }) {
    setNumPages(numPages);
  }

 const goToPrevPage = () =>
  {setPageNumber(pageNumber-1)};

  const goToNextPage = () =>
  {setPageNumber(pageNumber+1)};

  return (

    <div>
        <nav>
          <button onClick={goToPrevPage}>Prev</button>
          <button onClick={goToNextPage}>Next</button>
        </nav>
  
      <div style={{ width: 1000 }}>
      <Document file={pdfFile} onLoadSuccess={onDocumentLoadSuccess}>
        <Page pageNumber={pageNumber} width={1000}/>
      </Document>
      <p>
        Page {pageNumber} of {numPages}
      </p>
    </div>
    </div>
  );
}
export default Transcript;