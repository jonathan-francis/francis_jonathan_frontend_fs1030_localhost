import React from 'react'
import "./styles.css"
import './App.css'
import Navigation from './components/shared/Navigation'
import Footer from './components/shared/Footer'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from './components/pages/Home'
import Resume from './components/pages/Resume'
import CoverLetter from './components/subpages/CoverLetter'
import Transcript from './components/subpages/Transcript'
import Portfolio from './components/pages/Portfolio'
import Project1 from './components/subpages/Project1'
import Project2 from './components/subpages/Project2'
import TheoPage from './components/subpages/TheoPage'
import MickeyPage from './components/subpages/MickeyPage'
import OlliePage from './components/subpages/OlliePage'
import Entry from './components/pages/Entry'
import User from './components/pages/User'
import Login from './components/pages/Login'
import Submissions from './components/pages/Submissions'
import SubmissionsProfile from './components/pages/SubmissionsProfile'
import ResumeFeedbackForm from './components/pages/ResumeFeedbackForm'
import ResumeFeedbackFormList from './components/pages/ResumeFeedbackFormList'
import ResumeFeedbackFormListProfile from './components/pages/ResumeFeedbackFormListProfile'
import PortfolioFeedbackForm from './components/pages/PortfolioFeedbackForm'
import PortfolioFeedbackFormList from './components/pages/PortfolioFeedbackFormList'
import PortfolioFeedbackListProfile from './components/pages/PortfolioFeedbackListProfile'


function App() {
  return (
   <BrowserRouter>
        <Navigation />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/Resume" component={Resume} />
          <Route exact path="/Resume/CoverLetter" component={CoverLetter} />
          <Route exact path="/Resume/Transcript" component={Transcript} />
          <Route exact path="/Portfolio" component={Portfolio} />
          <Route exact path="/Portfolio/Project1" component={Project1} />
          <Route exact path="/Portfolio/Project2" component={Project2} />
          <Route exact path="/Portfolio/Project2/TheoPage" component={TheoPage} />
          <Route exact path="/Portfolio/Project2/MickeyPage" component={MickeyPage} />
          <Route exact path="/Portfolio/Project2/OlliePage" component={OlliePage} />
          <Route exact path="/Entry" component={Entry} />
          <Route exact path="/User" component= {User} />
          <Route exact path="/Login" component= {Login} />
          <Route exact path="/Submissions" component= {Submissions} />
          <Route exact path="/Submissions/:id" component= {SubmissionsProfile} />
          <Route exact path="/Resume/FeedbackForm" component= {ResumeFeedbackForm} />
          <Route exact path="/Resume/FeedbackForm/List" component= {ResumeFeedbackFormList} />
          <Route exact path="/Resume/FeedbackForm/List/:id" component= {ResumeFeedbackFormListProfile} />
          <Route exact path="/Portfolio/FeedbackForm" component= {PortfolioFeedbackForm} />
          <Route exact path="/Portfolio/FeedbackForm/List" component= {PortfolioFeedbackFormList} />
          <Route exact path="/Portfolio/FeedbackForm/List/:id" component= {PortfolioFeedbackListProfile} />
          </Switch>
        <Footer />  
    </BrowserRouter>
  )
}

export default App;
